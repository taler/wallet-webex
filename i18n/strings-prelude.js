if (!window.i18n) {
  throw Error("Module loading order incorrect, please load i18n module before loading i18n-strings");
}
